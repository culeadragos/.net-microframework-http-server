﻿namespace PWMServer.Services
{
    public interface IPulseService
    {
        double CalculateDurationSeconds(double frequency, double duty, out double offTime);
    }
}