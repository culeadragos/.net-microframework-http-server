﻿using PWMServer.Domain;

namespace PWMServer.Services
{
    public interface ICycleService
    {
        void Calibrate();
        CyclePulse GenerateCyclePulse(double frequency, double duty);

        int ConvertSecondsToForCycles(double seconds);
    }
}