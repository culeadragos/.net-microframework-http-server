using System;
using Microsoft.SPOT;
using PWMServer.Domain;

namespace PWMServer.Services
{
    public class DelayService : IDelayService
    {
        public DelayedPulse GeneratePulse(double frequencyInHz, double dutyDelayInPercents, double dutyInPercents)
        {
            var periodInSeconds = 1d / frequencyInHz;

            var delayInSeconds = periodInSeconds * dutyDelayInPercents / 100d;

            var dutyInSeconds = periodInSeconds * dutyDelayInPercents / 100d;

            return new DelayedPulse(delayInSeconds, dutyInSeconds);
        }
    }
}
