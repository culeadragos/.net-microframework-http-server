using System;
using Microsoft.SPOT;
using PWMServer.Domain;
using System.Threading;

namespace PWMServer.Service
{
    public class FiddleService : IFiddleService
    {
        private const double START_FREQUENCY = 1d;
        private const double END_FREQUENCY = 200000d;
        private const double START_DUTY = 0.1d;
        private const double END_DUTY = 0.25d;
        private const int STEP_MILLISECONDS = 10;
        private const double DUTY_STEP = 0.0001;
        private Timer timer;

        private double frequency;
        private double duty;
        private bool paused;
        private object pausedLock = new object();

        private bool Paused
        {
            get
            {
                lock(pausedLock)
                {
                    return paused;
                }
            }
            set
            {
                lock (pausedLock)
                {
                    paused = value;
                }
            }
        }

        public event FiddleEventHandler NewValue;

        public void Start()
        {
            frequency = START_FREQUENCY;
            duty = START_DUTY;

            if (timer != null)
            {
                throw new InvalidOperationException("Already running");
            }

            StartTimer();

        }

        private void StartTimer()
        {
            timer = new Timer(new TimerCallback((obj) =>
            {
                try
                {
                    timer.Change(Timeout.Infinite, Timeout.Infinite);

                    if (Paused)
                    {
                        return;
                    }

                    if (this.NewValue != null)
                    {
                        this.NewValue(this, new FiddleEventArgs(frequency, duty));
                    }

                    if (Increment())
                    {
                        timer.Change(Timeout.Infinite, Timeout.Infinite);
                    }
                    else
                    {
                        timer.Change(0, STEP_MILLISECONDS);
                    }
                }
                catch(Exception ex)
                {
                    Debug.Print(ex.Message + " | " + ex.StackTrace);
                }
            }), null, 0, STEP_MILLISECONDS);
        }

        public void Pause()
        {
            Paused = true;
        }

        public void Resume()
        {
            Paused = false;
            timer.Dispose();
            StartTimer();
        }

        public void Stop()
        {
            Paused = true;
            timer.Dispose();
            timer = null;
            Paused = false;
        }

        public void IncreaseDuty()
        {
            if (duty == END_DUTY)
            {
                return;
            }

            Pause();
            duty += DUTY_STEP;
            frequency = START_FREQUENCY;
            Resume();
        }

        private bool Increment()
        {
            if (frequency == END_FREQUENCY)
            {
                frequency = START_FREQUENCY;

                if (duty == END_DUTY)
                {
                    duty = START_DUTY;
                    return true;
                }

                duty += DUTY_STEP;
            }

            frequency += 1d;
            return false;
        }
    }
}
