using System;
using Microsoft.SPOT;

namespace PWMServer.Services
{
    public class PulseService : IPulseService
    {

        /// <summary>
        /// Calculates the duration of the pulse based on frequency and duty
        /// </summary>
        /// <param name="frequency">In Hz</param>
        /// <param name="duty">Duty where 1d means 100%</param>
        /// <returns>Seconds</returns>
        public double CalculateDurationSeconds(double frequency, double duty, out double offTime)
        {
            var period = 1d / frequency;

            offTime = (1 - duty) * period; 

            return duty * period;
        }
    }
}
