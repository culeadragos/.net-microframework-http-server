using System;
using Microsoft.SPOT;
using NetMFUtils;
using NetMFUtils.CycleTimers;
using PWMServer.Domain;

namespace PWMServer.Services
{
    public class CycleService : ICycleService
    {
        private readonly IPulseService pulseService;
        private readonly SumCycleTimer sumCycleTimer = new SumCycleTimer();
        private readonly AssignCycleTimer assignCycleTimer = new AssignCycleTimer();
        private readonly WriteCycleTimer writeCycleTimer = new WriteCycleTimer();
        private readonly ForCycleTimer forCycleTimer = new ForCycleTimer();

        public CycleService(IPulseService pulseService)
        {
            this.pulseService = pulseService;
        }

        public void Calibrate()
        {
            //sumCycleTimer.Calibrate();
            //assignCycleTimer.Calibrate();
            //writeCycleTimer.Calibrate();
            forCycleTimer.Calibrate();

            //Debug.Print("Sum cycles per second: " + sumCycleTimer.CyclesPerSecond);
            //Debug.Print("Assign cycle in seconds: " + assignCycleTimer.SecondsPerCycle);
            //Debug.Print("Write cycle in seconds: " + writeCycleTimer.CyclesPerSecond);
        }

        public CyclePulse GenerateCyclePulse(double frequency, double duty)
        {
            double offTime;
            var durationSeconds = this.pulseService.CalculateDurationSeconds(frequency, duty, out offTime);

            var onCyclesAsDouble = forCycleTimer.CyclesPerSecond * durationSeconds;

            //how many sum cycles are in off time subtract the time corresponding to an assign cycle
            var offCyclesAsDouble = forCycleTimer.CyclesPerSecond * offTime;

            var result = new CyclePulse((int)onCyclesAsDouble, (int)offCyclesAsDouble);
            return result;
        }

        public int ConvertSecondsToForCycles(double seconds)
        {
            var result = seconds / this.forCycleTimer.SecondsPerCycle;

            return (int)result;
        }
    }
}
