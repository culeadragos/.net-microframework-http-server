﻿using PWMServer.Domain;

namespace PWMServer.Service
{
    public interface IFiddleService
    {
        event FiddleEventHandler NewValue;

        void Pause();
        void Resume();
        void Start();
        void Stop();
        void IncreaseDuty();
    }
}