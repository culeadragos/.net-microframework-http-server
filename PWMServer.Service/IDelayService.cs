﻿using PWMServer.Domain;

namespace PWMServer.Services
{
    public interface IDelayService
    {
        DelayedPulse GeneratePulse(double frequencyInHz, double dutyDelayInPercents, double dutyInPercents);
    }
}