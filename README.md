# .Net Microframework Http Server

Easy to use piece of software to manage any NETMF microcontroller

An example for Netduino 3 board is included, but it could be easily adapted to any .Net Microframework board without touching the NetMFHttpServer project.

For a complete guide see [Wiki](https://bitbucket.org/culeadragos/.net-microframework-http-server/wiki/Home)

Development with VS 2015, netmfvs14.vsix, MicroFrameworkSDK.MSI .