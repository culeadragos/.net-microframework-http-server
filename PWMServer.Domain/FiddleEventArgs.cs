using System;
using Microsoft.SPOT;

namespace PWMServer.Domain
{
    public delegate void FiddleEventHandler(object sender, FiddleEventArgs e);

    public class FiddleEventArgs
    {
        public double Frequency { get; private set; }

        public double Duty { get; private set; }

        public FiddleEventArgs(double frequency, double duty)
        {
            this.Frequency = frequency;
            this.Duty = duty;
        }
    }
}
