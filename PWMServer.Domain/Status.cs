using System;
using Microsoft.SPOT;

namespace PWMServer.Domain
{
    public class Status
    {
        public double Frequency { get; }

        public double PrimaryDuty { get; }

        public double SecondaryDutyDelay { get; }

        public double SecondaryDuty { get; }

        public double TertiaryDutyDelay { get; }

        public double TertiaryDuty { get; }

        public Status(double frequency, double primaryDuty, double secondaryDutyDelay, double secondaryDuty, double tertiaryDutyDelay, double tertiaryDuty)
        {
            this.Frequency = frequency;
            this.PrimaryDuty = primaryDuty;
            this.SecondaryDutyDelay = secondaryDutyDelay;
            this.SecondaryDuty = secondaryDuty;
            this.TertiaryDutyDelay = tertiaryDutyDelay;
            this.TertiaryDuty = tertiaryDuty;
        }
    }
}
