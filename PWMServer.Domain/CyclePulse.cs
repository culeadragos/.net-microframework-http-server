using System;
using Microsoft.SPOT;

namespace PWMServer.Domain
{
    public class CyclePulse
    {
        public int OnCycles { get; }

        public int OffCycles { get; }

        public CyclePulse(int onCycles, int offCycles)
        {
            this.OnCycles = onCycles;
            this.OffCycles = offCycles;
        }
    }
}
