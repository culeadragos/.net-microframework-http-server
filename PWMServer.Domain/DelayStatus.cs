using System;
using Microsoft.SPOT;

namespace PWMServer.Domain
{
    public class DelayStatus
    {
        public double Frequency { get; }

        public double PrimaryDuty { get; }

        public int SecondaryCyclesDelay { get; }

        public double SecondaryDuty { get; }

        public DelayStatus(double frequency, double primaryDuty, int secondaryCyclesDelay, double secondaryDuty)
        {
            this.Frequency = frequency;
            this.PrimaryDuty = primaryDuty;
            this.SecondaryCyclesDelay = secondaryCyclesDelay;
            this.SecondaryDuty = secondaryDuty;
        }
    }
}
