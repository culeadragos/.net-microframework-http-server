using System;
using Microsoft.SPOT;

namespace PWMServer.Domain
{
    public class DelayedPulse
    {
        public double DelaySeconds { get; }

        public double DurationSeconds { get; }

        public DelayedPulse(double delaySeconds, double durationSeconds)
        {
            this.DelaySeconds = delaySeconds;
            this.DurationSeconds = durationSeconds;
        }
    }
}
