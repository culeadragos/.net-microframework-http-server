using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using PWMServer.Domain;

namespace NetduinoDelayedPWM.Controllers
{
    public class TertiaryPortController : BaseController
    {
        public void ChangePulse(string dutyDelay, string duty)
        {
            double dutyDelayAsDouble;
            if (!PwmParsing.TryParseDuty(dutyDelay, out dutyDelayAsDouble))
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangeTertiaryPulse(dutyDelayAsDouble, dutyAsDouble);
        }

        public Status IncreaseDutyDelayBy(string value)
        {
            var delayAsDouble = double.Parse(value);

            var newDelay = Program.TertiaryDutyDelay + delayAsDouble;
            if (newDelay >= 0.999 || newDelay < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new Status(Program.Frequency, Program.PrimaryDuty, Program.SecondaryDutyDelay, Program.SecondaryDuty,
                newDelay, Program.TertiaryDuty);

            Program.ChangeTertiaryPulse(newDelay, Program.TertiaryDuty);

            return result;
        }

        public Status IncreaseDutyBy(string value)
        {
            var dutyAsDouble = double.Parse(value);

            var newDuty = Program.TertiaryDuty + dutyAsDouble;
            if (newDuty >= 0.999 || newDuty < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new Status(Program.Frequency, Program.PrimaryDuty, Program.SecondaryDutyDelay, Program.SecondaryDuty,
                Program.TertiaryDutyDelay, newDuty);

            Program.ChangeTertiaryPulse(Program.TertiaryDutyDelay, newDuty);

            return result;
        }
    }
}
