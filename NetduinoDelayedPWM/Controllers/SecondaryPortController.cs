using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using PWMServer.Domain;

namespace NetduinoDelayedPWM.Controllers
{
    public class SecondaryPortController:BaseController
    {
        public void ChangePulse(string dutyDelay, string duty)
        {
            double dutyDelayAsDouble;
            if (!PwmParsing.TryParseDuty(dutyDelay, out dutyDelayAsDouble))
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangeSecondaryPulse(dutyDelayAsDouble, dutyAsDouble);
        }

        public Status IncreaseDutyDelayBy(string value)
        {
            var delayAsDouble = double.Parse(value);

            var newDelay = Program.SecondaryDutyDelay + delayAsDouble;
            if (newDelay >= 0.999 || newDelay < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new Status(Program.Frequency, Program.PrimaryDuty, newDelay, Program.SecondaryDuty,
                Program.TertiaryDutyDelay, Program.TertiaryDuty);

            Program.ChangeSecondaryPulse(newDelay, Program.SecondaryDuty);

            return result;
        }

        public Status IncreaseDutyBy(string value)
        {
            var dutyAsDouble = double.Parse(value);

            var newDuty = Program.SecondaryDuty + dutyAsDouble;
            if (newDuty >= 0.999 || newDuty < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new Status(Program.Frequency, Program.PrimaryDuty, Program.SecondaryDutyDelay, newDuty,
                Program.TertiaryDutyDelay, Program.TertiaryDuty);

            Program.ChangeSecondaryPulse(Program.SecondaryDutyDelay, newDuty);

            return result;
        }
    }
}
