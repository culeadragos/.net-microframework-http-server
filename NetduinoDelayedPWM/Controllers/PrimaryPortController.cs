using System;
using Microsoft.SPOT;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer;
using PWMServer.Domain;

namespace NetduinoDelayedPWM.Controllers
{
    public class PrimaryPortController:BaseController
    {
        public void ChangePulse(string frequency, string duty)
        {
            double frequencyAsDouble;
            if (!PwmParsing.TryParseFrequency(frequency, out frequencyAsDouble))
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangePrimaryPulse(frequencyAsDouble, dutyAsDouble);
        }

        public Status IncreaseFrequencyBy(string value)
        {
            var frequencyAsDouble = double.Parse(value);

            var newFrequency = Program.Frequency + frequencyAsDouble;
            if (newFrequency >= 24000000 || newFrequency < 0.1)
            {
                throw new BadRequestException();
            }

            var result = new Status(newFrequency, Program.PrimaryDuty, Program.SecondaryDutyDelay, Program.SecondaryDuty,
                Program.TertiaryDutyDelay, Program.TertiaryDuty);

            Program.ChangePrimaryPulse(newFrequency, Program.PrimaryDuty);

            return result;
        }

        public Status IncreaseDutyBy(string value)
        {
            var dutyyAsDouble = double.Parse(value);

            var newDuty = Program.PrimaryDuty + dutyyAsDouble;
            if (newDuty >= 0.999 || newDuty < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new Status(Program.Frequency, newDuty, Program.SecondaryDutyDelay, Program.SecondaryDuty,
                Program.TertiaryDutyDelay, Program.TertiaryDuty);

            Program.ChangePrimaryPulse(Program.Frequency, newDuty);

            return result;
        }
    }
}
