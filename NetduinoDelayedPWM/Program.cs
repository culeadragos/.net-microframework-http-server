using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using System.Threading;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using NetMFUtils;
using NetduinoDelayedPWM.Controllers;
using PWMServer.Services;

namespace NetduinoDelayedPWM
{
    public class Program
    {
        private static HttpServer httpServer = new HttpServer(81);
        private static Thread httpServerThread;
        private static AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        private static Cpu.PWMChannel primaryChannel = PWMChannels.PWM_PIN_D6;
        private static Cpu.PWMChannel secondaryChannel = PWMChannels.PWM_PIN_D9;
        private static Cpu.PWMChannel tertiaryChannel = PWMChannels.PWM_PIN_D10;
        private static double _frequency = 250d;
        private static double _primaryDuty = 0.1d;
        private static double _secondaryDuty = 0.15;
        private static double _secondaryDutyDelay = 0.005;

        private static Microsoft.SPOT.Hardware.PWM primaryPort = new PWM(primaryChannel, _frequency, _primaryDuty, false);
        private static Microsoft.SPOT.Hardware.PWM secondaryPort = new PWM(secondaryChannel, _frequency, _secondaryDuty, false);
        
        private static int _secondaryDelayCycles;
        private static ICycleService cycleService;
        private static IPulseService pulseService;

        public static double Frequency
        {
            get { return _frequency; }
        }

        public static double PrimaryDuty
        {
            get { return _primaryDuty; }
        }

        public static double SecondaryDutyDelay
        {
            get { return _secondaryDutyDelay; }
        }

        public static double SecondaryDuty
        {
            get { return _secondaryDuty; }
        }

        public static double TertiaryDutyDelay
        {
            get { return 0d; }
        }

        public static double TertiaryDuty
        {
            get { return 0d; }
        }

        public static void Main()
        {
            PopulateTheRegistry();

            cycleService = (ICycleService)Registry.Get(typeof(ICycleService));
            pulseService = (IPulseService)Registry.Get(typeof(IPulseService));

            httpServerThread = new Thread(() =>
            {
                httpServer.Start();
            });
            httpServerThread.Start();

            var calculationThread = new Thread(() =>
            {
                while (true)
                {
                    autoResetEvent.WaitOne();

                    StopPWM();
                    CalculateDelays();
                    StartPWM();
                }
            });
            calculationThread.Start();

            Thread.Sleep(100);
            cycleService.Calibrate();

            
            CalculateDelays();

            StartPWM();

            Thread.Sleep(Timeout.Infinite);
        }

        public static void ChangePrimaryPulse(double frequency, double duty)
        {
            _frequency = frequency;
            _primaryDuty = duty;

            autoResetEvent.Set();

            Thread.Sleep(10);//sleep a little to allow the main thread to restart the pwm correctly
        }

        public static void ChangeSecondaryPulse(double dutyDelay, double duty)
        {
            _secondaryDuty = duty;
            _secondaryDutyDelay = dutyDelay;

            autoResetEvent.Set();

            Thread.Sleep(10);//sleep a little to allow the main thread to restart the pwm correctly
        }

        public static void ChangeTertiaryPulse(double dutyDelay, double duty)
        {
            autoResetEvent.Set();

            Thread.Sleep(10);//sleep a little to allow the main thread to restart the pwm correctly
        }

        private static void PopulateTheRegistry()
        {
            var pulseService = new PulseService();
            Registry.RegisterInstance(typeof(IPulseService), pulseService);
            var cycleService = new CycleService(pulseService);
            Registry.RegisterInstance(typeof(ICycleService), cycleService);
            Registry.RegisterInstance(typeof(IDelayService), new DelayService());
        }

        private static void CalculateDelays()
        {
            double offTime;
            double secondaryDelaySeconds = pulseService.CalculateDurationSeconds(_frequency, _secondaryDutyDelay, out offTime);
            _secondaryDelayCycles = cycleService.ConvertSecondsToForCycles(secondaryDelaySeconds);
        }

        private static void StartPWM()
        {
            primaryPort.Frequency = _frequency;
            primaryPort.DutyCycle = _primaryDuty;
            secondaryPort.Frequency = _frequency;
            secondaryPort.DutyCycle = _secondaryDuty;

            primaryPort.Start();
            for (var i = 0; i < _secondaryDelayCycles; i++)
            {
            }
            secondaryPort.Start();
        }

        private static void StopPWM()
        {
            primaryPort.Stop();
            secondaryPort.Stop();
        }
    }
}
