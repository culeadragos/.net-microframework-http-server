using System;
using System.Collections;
using System.Net;
using System.Reflection;
using Json.NETMF;
using Microsoft.SPOT;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer.Model;

namespace NetMFHttpServer
{
    internal class ActionDispatcher
    {
        private readonly JsonSerializer _jsonSerializer;

        public ActionDispatcher(JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }
        internal object Dispatch(Route route, ArrayList parameters)
        {
            ConstructorInfo controllerConstructor;

            try
            {
                controllerConstructor = route.ControllerType.GetConstructor(new Type[] { });
            }
            catch (Exception e)
            {
                Debug.Print(e.Message);
                throw new NoParameterlessConstructorException(route.ControllerType);
            }
           
            var controller = (BaseController)controllerConstructor.Invoke(new object[] { });
            controller.SetJsonSerializer(_jsonSerializer);

            //var parametersArray = new object[parameters.Values.Count];
            //var j = 0;
            //foreach (var pair in parameters)
            //{
            //    parametersArray[j] = pair;
            //    j++;
            //}
            
            //parameters.Values.CopyTo(parametersArray, 0);

            object resultValue = null;

            try
            {
                resultValue = route.Action.Invoke(controller, parameters.ToArray());
            }
            catch (Exception e)
            {
                Debug.Print(e.Message + " " + e.StackTrace);

                throw new ActionException();
            }

            return resultValue;
        }
    }
}
