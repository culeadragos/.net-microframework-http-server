using System;
using Microsoft.SPOT;

namespace NetMFHttpServer.Model
{
    internal enum ContentType
    {
        Undefined = 0,
        ApplicationJson = 1
    }
}
