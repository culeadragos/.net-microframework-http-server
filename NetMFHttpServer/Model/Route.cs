using System;
using System.Reflection;

namespace NetMFHttpServer.Model
{
    public class Route
    {
        internal Type ControllerType { get; }
        internal MethodInfo Action { get; }

        internal Verb Verb { get; }

        internal string ControllerName { get;  }

        /// <summary>
        /// Much faster to use this for filtering instead of reflection when routing
        /// </summary>
        internal string ActionName { get;  }

        internal string ControllerNameLowerCase { get; }

        internal string ActionNameLowerCase { get; }

        public Route(Type controllerType, MethodInfo action, Verb verb, string controllerName)
        {
            ControllerType = controllerType;
            Action = action;
            Verb = verb;
            ControllerName = controllerName;
            ControllerNameLowerCase = controllerName.ToLower();
            ActionName = action.Name;
            ActionNameLowerCase = action.Name.ToLower();
        }
    }
}
