using System;
using Microsoft.SPOT;

namespace NetMFHttpServer.Model
{
    public enum Verb
    {
        Undefined  = 0,
        GET = 1,
        POST = 2,
        PUT = 3,
        DELETE = 4
    }
}
