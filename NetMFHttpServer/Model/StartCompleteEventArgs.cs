using System;
using Microsoft.SPOT;

namespace NetMFHttpServer.Model
{
    public class StartCompleteEventArgs:EventArgs
    {
        public string Address { get; }

        public StartCompleteEventArgs(string address)
        {
            Address = address;
        }
    }
}
