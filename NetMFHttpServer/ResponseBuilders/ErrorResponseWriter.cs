using System;
using System.Net;
using Microsoft.SPOT;
using NetMFHttpServer.Exceptions;

namespace NetMFHttpServer.ResponseBuilders
{
    internal class ErrorResponseWriter
    {
        internal void Write(HttpStatusCode statusCode, HttpListenerResponse response)
        {
            response.StatusCode = (int)statusCode;
            response.OutputStream.Close();
        }
    }
}
