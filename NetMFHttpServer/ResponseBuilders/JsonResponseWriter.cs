using System;
using System.Net;
using Json.NETMF;
using Microsoft.SPOT;

namespace NetMFHttpServer.ResponseBuilders
{
    internal class JsonResponseWriter
    {
        private readonly JsonSerializer _serializer;

        public JsonResponseWriter(JsonSerializer serializer)
        {
            _serializer = serializer;
        }

        internal void Write(object result, HttpListenerResponse response)
        {
            var json = result != null ? _serializer.Serialize(result) : "{}";

            response.ContentType = "application/json";

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(json);

            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);

            // must close the output stream.
            output.Close();
        }
    }
}
