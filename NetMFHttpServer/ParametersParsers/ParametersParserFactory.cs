using System;
using System.Collections;
using System.Net;
using Microsoft.SPOT;

namespace NetMFHttpServer.ParametersParsers
{
    internal interface IParametersParser
    {
        ArrayList ParseParameters(string originalString, string body);
    }
}
