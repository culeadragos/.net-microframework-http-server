using System;
using System.Net;
using Microsoft.SPOT;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer.Model;

namespace NetMFHttpServer.ParametersParsers
{
    internal class ParametersParserStrategy
    {
        private readonly ParametersParserForGetRequest _parametersParserForGetRequest;

        public ParametersParserStrategy(ParametersParserForGetRequest parametersParserForGetRequest)
        {
            _parametersParserForGetRequest = parametersParserForGetRequest;
        }

        internal IParametersParser CreateParser(Route route)
        {
            switch (route.Verb)
            {
                case Verb.GET:
                    return _parametersParserForGetRequest;

                default:
                    throw new InvalidVerbException();
            }
        }
    }
}
