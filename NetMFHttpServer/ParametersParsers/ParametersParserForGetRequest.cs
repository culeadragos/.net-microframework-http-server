using System;
using System.Collections;
using System.Net;

namespace NetMFHttpServer.ParametersParsers
{
    internal class ParametersParserForGetRequest:IParametersParser
    {
        public ArrayList ParseParameters(string originalString, string body)
        {
            var toReturn = new ArrayList();

            if (originalString.IndexOf("?") < 0)
            {
                return toReturn;
            }

            var queryString = originalString.Split('?')[1];
            

            var keysWithValues = queryString.Split('&');

            foreach (var keyWithValue in keysWithValues)
            {
                var parts = keyWithValue.Split('=');

                toReturn.Add(parts[1]);
            }

            return toReturn;
        }
    }
}
