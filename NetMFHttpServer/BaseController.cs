using System;
using System.Net;
using Json.NETMF;
using Microsoft.SPOT;

namespace NetMFHttpServer
{
    /// <summary>
    /// The actions must receive only string parameters because of the limitations in reading parameters metadata with Reflection in NETMF
    /// Only GET verb is supported for now
    /// Also, parameters names are not taken into account. The parameters matching is done based on order because of the same Reflection limitation
    /// </summary>
    public abstract class BaseController
    {
        //protected HttpListenerRequest Request { get; private set; }

        //internal void SetRequest(HttpListenerRequest request)
        //{
        //    Request = request;
        //}

        private JsonSerializer _jsonSerializer;

        internal void SetJsonSerializer(JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        /// <summary>
        /// Deserializes a json string into a hashtable. JSON.NETMF does not support any kind of strongly typed deserialization.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        protected object DeserializeJson(string json)
        {
            var result = _jsonSerializer.Deserialize(json);
            return result;
        }
    }
}
