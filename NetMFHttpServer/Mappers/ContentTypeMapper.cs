using System;
using NetMFHttpServer.Model;

namespace NetMFHttpServer.Mappers
{
    internal static class ContentTypeMapper
    {
        internal static ContentType Map(string contentType)
        {
            switch (contentType)
            {
                case "application/json":
                    return ContentType.ApplicationJson;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
