using System;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer.Model;

namespace NetMFHttpServer.Mappers
{
    internal static class VerbMapper
    {
        internal static Verb Map(string verb)
        {
            switch (verb)
            {
                case "GET":
                    return Verb.GET;

                case "POST":
                    return Verb.POST;

                case "DELETE":
                    return Verb.DELETE;

                case "PUT":
                    return Verb.PUT;

                default:
                    throw new InvalidVerbException();
            }
        }
    }
}
