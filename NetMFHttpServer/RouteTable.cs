
using System;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer.Model;
using System.Collections;

namespace NetMFHttpServer
{
    public class RouteTable
    {
        private readonly Route[] _routes;

        internal RouteTable(Route[] routes)
        {
            _routes = routes;
        }

        internal Route GetRoute(string controller, string action, Verb verb)
        {
            var controllerLowerCase = controller.ToLower();
            var actionLowerCase = action.ToLower();

            foreach (var route in _routes)
            {
                if (route.ControllerNameLowerCase == controllerLowerCase && route.ActionNameLowerCase == actionLowerCase && route.Verb == verb)
                {
                    return route;
                }
            }

            throw new RouteNotFoundException();
        }

        public string[] Routes
        {
            get
            {
                var result = new string[_routes.Length];

                for (var i =0; i < _routes.Length;i++)
                {
                    result[i] = _routes[i].ControllerNameLowerCase + "/" + _routes[i].ActionNameLowerCase;
                }

                return result;
            }
        }
    }
}
