using System;
using System.Net;
using Microsoft.SPOT;
using NetMFHttpServer.Model;

namespace NetMFHttpServer.Exceptions
{
    public abstract class BaseException:ApplicationException
    {
        public abstract HttpStatusCode StatusCode { get; }
    }
}
