using System;
using System.Net;
using Microsoft.SPOT;

namespace NetMFHttpServer.Exceptions
{
    internal class ActionException:BaseException
    {
        public override HttpStatusCode StatusCode
        {
            get
            {
                return HttpStatusCode.BadRequest;
            }
        }
    }
}
