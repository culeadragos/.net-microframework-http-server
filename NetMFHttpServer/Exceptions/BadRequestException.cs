using System;
using System.Net;
using Microsoft.SPOT;

namespace NetMFHttpServer.Exceptions
{
    public class BadRequestException : BaseException
    {
        public override HttpStatusCode StatusCode
        {
            get
            {
                return HttpStatusCode.BadRequest;
            }
        }
    }
}
