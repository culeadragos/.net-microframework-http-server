using System;
using System.Net;
using Microsoft.SPOT;
using NetMFHttpServer.Model;

namespace NetMFHttpServer.Exceptions
{
    internal class NoParameterlessConstructorException:BaseException
    {
        internal Type Type { get; private set; }


        internal NoParameterlessConstructorException(Type type)
        {
            Type = type;
        }

        public override HttpStatusCode StatusCode
        {
            get
            {
                return HttpStatusCode.BadRequest;
            }
        }
    }
}
