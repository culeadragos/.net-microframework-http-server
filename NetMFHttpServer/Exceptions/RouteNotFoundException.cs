using System;
using System.Net;
using Microsoft.SPOT;
using NetMFHttpServer.Model;

namespace NetMFHttpServer.Exceptions
{
    internal class RouteNotFoundException:BaseException
    {
        public override HttpStatusCode StatusCode
        {
            get
            {
                return HttpStatusCode.NotFound;
            }
        }
    }
}
