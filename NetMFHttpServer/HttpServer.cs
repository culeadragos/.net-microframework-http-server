using System;
using System.Net;
using System.Threading;
using Json.NETMF;
using Microsoft.SPOT;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer.Mappers;
using NetMFHttpServer.Model;
using NetMFHttpServer.ParametersParsers;
using NetMFHttpServer.ResponseBuilders;

namespace NetMFHttpServer
{
    /// <summary>
    /// Supports MVC/Web Api style controller-action 
    /// </summary>
    public class HttpServer
    {
        private readonly int _port;
        private readonly HttpListener _listener;
        private readonly ControllerAssemblyParser _controllerAssemblyParser;
        private readonly ControllerTypeParser _controllerTypeParser = new ControllerTypeParser();
        private readonly Router _router= new Router();
        private readonly ParametersParserStrategy _parametersParserStrategy;
        private readonly ActionDispatcher _actionDispatcher;
        private readonly JsonResponseWriter _jsonResponseWriter;
        private readonly ErrorResponseWriter _errorResponseWriter;
        private readonly JsonSerializer _serializer = new JsonSerializer();

        /// <summary>
        /// Fired right before blocking the thread and start listening for requests. 
        /// When handling this event, a tiny sleep period is needed before making requests.
        /// </summary>
        public event StartCompleteEventHandler StartComplete;

        public delegate void StartCompleteEventHandler(HttpServer server, StartCompleteEventArgs e);

        public HttpServer(int port)
        {
            _port = port;
            _listener = new HttpListener("http", _port);
            
            _controllerAssemblyParser = new ControllerAssemblyParser(_controllerTypeParser);
            
            var parametersParserForGetRequest = new ParametersParserForGetRequest();
            _parametersParserStrategy = new ParametersParserStrategy(parametersParserForGetRequest);

            _actionDispatcher = new ActionDispatcher(_serializer);
            _jsonResponseWriter = new JsonResponseWriter(_serializer);
            _errorResponseWriter = new ErrorResponseWriter();
        }

        public void Start()
        {
            RouteTable routeTable = null;
            try
            {
                routeTable = _controllerAssemblyParser.PopulateRouteTable();
                Debug.Print("Route table: " + _serializer.Serialize(routeTable));
            }
            catch (Exception e)
            {
                Debug.Print("Starting http server failed: " + e.Message + " " + e.StackTrace);
                return;
            }

            int maxIntervalCount = 100;
            int count = 0;

            while (IPAddress.GetDefaultLocalAddress() == IPAddress.Any && count < maxIntervalCount)
            {
                Debug.Print("Sleep while obtaining an IP");
                Thread.Sleep(10);
                count++;
            };

            _listener.Start();
            var ipAddress = IPAddress.GetDefaultLocalAddress();
            var httpAddress = string.Concat("http://", ipAddress.ToString(), ":", _port);
            Debug.Print(string.Concat("Listening at: ", httpAddress, " ..."));
            StartComplete?.Invoke(this, new StartCompleteEventArgs(httpAddress));

            while (true)
            {
                // Note: The GetContext method blocks while waiting for a request. 
                HttpListenerContext context = _listener.GetContext();

                HandleRequest(context, routeTable);
                
                //Debug.Print("" + request.InputStream.Read());
                // Obtain a response object.
            }

        }

        public void Stop()
        {
            _listener.Stop();
        }


        private void HandleRequest(HttpListenerContext context, RouteTable routeTable)
        {
            HttpListenerRequest request = context.Request;

            
            //var contentType = ContentTypeMapper.Map(request.ContentType);
            var verb = VerbMapper.Map(request.HttpMethod);
            HttpStatusCode statusCode;
            var actionResult = HandleRoute(routeTable, request.Url.OriginalString, verb, string.Empty, out statusCode);
            if (statusCode == HttpStatusCode.OK)
            {
                _jsonResponseWriter.Write(actionResult, context.Response);
            }
            else
            {
                _errorResponseWriter.Write(statusCode, context.Response);
            }
        }

        /// <summary>
        /// Not intended to be called directly. It's exposed just for unit testing
        /// </summary>
        /// <param name="routeTable"></param>
        /// <param name="originalString"></param>
        /// <param name="verb"></param>
        /// <param name="body"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public object HandleRoute(RouteTable routeTable, string originalString, Verb verb, string body, out HttpStatusCode statusCode)
        {
            if (originalString == "/")//TODO handle /favicon.ico
            {
                statusCode = HttpStatusCode.OK;
                return routeTable.Routes;
            }

            try
            {
                var route = _router.MapToRoute(routeTable, originalString, verb);
                var parametersParser = _parametersParserStrategy.CreateParser(route);
                var parametersTable = parametersParser.ParseParameters(originalString, body);
                var actionResult = _actionDispatcher.Dispatch(route, parametersTable);
                statusCode = HttpStatusCode.OK;
                return actionResult;
            }
            catch (BaseException e)
            {
                statusCode = e.StatusCode;

                Debug.Print(e.Message + " " + e.StackTrace);
            }
            catch (Exception e)
            {
                statusCode = HttpStatusCode.InternalServerError;

                Debug.Print(e.Message + " " + e.StackTrace);
            }

            return null;
        }
    }
}
