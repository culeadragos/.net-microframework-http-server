using System;
using Microsoft.SPOT;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer.Model;

namespace NetMFHttpServer
{
    public class Router
    {
        public Route MapToRoute(RouteTable routeTable, string originalString, Verb verb)
        {
            var relativeUrl = originalString.Split('?')[0];

            relativeUrl = relativeUrl.TrimStart('/').TrimEnd('/');

            var parts = relativeUrl.Split('/');

            if (parts.Length != 2)
            {
                throw new InvalidUrlException();
            }

            return routeTable.GetRoute(parts[0], parts[1], verb);
        }
    }
}
