using System;
using System.Collections;
using System.Reflection;
using NetMFHttpServer.Model;

namespace NetMFHttpServer
{
    /// <summary>
    /// Parses the metadata of the controller type to extract the methods and controllers. Unfortunately, there is no way to get the parameters of the methods, 
    /// so a proper model binding process cannot be done
    /// </summary>
    public class ControllerTypeParser
    {
        private readonly Type _typeOfObject = typeof(object); 

        internal void ExtractRoutes(Type controllerType, ArrayList routes)
        {
            var actionMethods = controllerType.GetMethods(BindingFlags.Public | BindingFlags.Instance);

            foreach (var actionMethod in actionMethods)
            {
                if (actionMethod.DeclaringType == _typeOfObject)
                {
                    continue;
                }

                var controllerName = GetControllerName(controllerType);

                var route = new Route(controllerType, actionMethod, Verb.GET, controllerName);

                routes.Add(route);
            }
        }

        private string GetControllerName(Type controllerType)
        {
            var index = controllerType.Name.LastIndexOf("Controller");

            var controllerName = controllerType.Name.Substring(0, index);

            return controllerName;
        }
    }
}
