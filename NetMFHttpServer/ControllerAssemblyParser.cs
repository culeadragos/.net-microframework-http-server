using System;
using System.Collections;
using NetMFHttpServer.Model;
using System.Reflection;

namespace NetMFHttpServer
{
    /// <summary>
    /// Extract all controllers, actions and parameters of all classes that inherit BaseController, using Reflection
    /// </summary>
    public class ControllerAssemblyParser
    {
        private Type _baseControllerType;
        private Type _typeOfType;
        private readonly ControllerTypeParser _controllerTypeParser;

        public ControllerAssemblyParser(ControllerTypeParser controllerTypeParser)
        {
            _controllerTypeParser = controllerTypeParser;
            _baseControllerType = typeof(BaseController);
            _typeOfType = typeof(Type);
        }

        public RouteTable PopulateRouteTable()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var controllerTypes = GetAllControllerTypes(assemblies);

            ArrayList allRoutes = new ArrayList();

            foreach (var controllerType in controllerTypes)
            {
                _controllerTypeParser.ExtractRoutes(controllerType, allRoutes);
            }

            var routes = (Route[]) allRoutes.ToArray(typeof(Route));
            return new RouteTable(routes);
        }

        private void AddAllControllerTypes(Assembly assembly, ArrayList controllerTypes)
        {
            var types = assembly.GetTypes();

            foreach (var type in types)
            {
                if (type.IsClass && type.IsSubclassOf(_baseControllerType))
                {
                    controllerTypes.Add(type);
                }
            }
        }

        private Type[] GetAllControllerTypes(Assembly[] assemblies)
        {
            ArrayList toReturn = new ArrayList();

            foreach (var assembly in assemblies)
            {
                AddAllControllerTypes(assembly, toReturn);
            }

            return (Type[])toReturn.ToArray(_typeOfType);
        }
    }
}
