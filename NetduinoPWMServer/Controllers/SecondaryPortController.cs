using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using PWMServer.Domain;

namespace NetduinoPWMServer.Controllers
{
    public class SecondaryPortController:BaseController
    {
        public void ChangePulse(string dutyDelay, string duty)
        {
            double dutyDelayAsDouble;
            if (!PwmParsing.TryParseDuty(dutyDelay, out dutyDelayAsDouble))
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangeSecondaryPulse(dutyDelayAsDouble);
        }

        public Status IncreaseDutyDelayBy(string value)
        {
            var delayAsDouble = double.Parse(value);

            var newDelay = Program.SecondaryDutyDelay + delayAsDouble;
            if (newDelay >= 0.999 || newDelay < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new Status(Program.Frequency, Program.PrimaryDuty, newDelay, 0, 0, 0);

            Program.ChangeSecondaryPulse(newDelay);

            return result;
        }
    }
}
