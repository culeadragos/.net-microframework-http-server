using System;
using Microsoft.SPOT;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using NetMFHttpServer;
using PWMServer.Domain;
using PWMServer.Services;

namespace NetduinoPWMServer.Controllers
{
    public class PwmController: BaseController
    {
        private IDelayService delayService = (IDelayService)Registry.Get(typeof(IDelayService));

        //public void SetPulse(string dutyDelay, string duty)
        //{
        //    double dutyDelayAsDouble;

        //    if (!PwmParsing.TryParseDuty(dutyDelay, out dutyDelayAsDouble))
        //    {
        //        throw new BadRequestException();
        //    }

        //    double dutyAsDouble;

        //    if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
        //    {
        //        throw new BadRequestException();
        //    }

        //    var pulse = delayService.GeneratePulse(Program.Frequency, dutyDelayAsDouble, dutyAsDouble);
        //}

        public Status GetStatus()
        {
            return new Status(Program.Frequency, Program.PrimaryDuty, Program.SecondaryDutyDelay, 0,
                0, 0);
        }
    }
}
