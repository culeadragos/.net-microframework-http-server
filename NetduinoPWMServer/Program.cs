﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using NetMFHttpServer;
using PWMServer.Services;
using NetMFUtils;

namespace NetduinoPWMServer
{
    public class Program
    {
        private static HttpServer httpServer = new HttpServer(81);
        private static Thread httpServerThread;
        private static AutoResetEvent calculationStartEvent = new AutoResetEvent(false);
        private static bool calculationDone;
        private static Cpu.PWMChannel primaryChannel = PWMChannels.PWM_PIN_D6;
        
        private static double _frequency = 250d;
        private static double _primaryDuty = 0.1d;
        private static double _secondaryDelay = 0.005;

        

        private static Microsoft.SPOT.Hardware.PWM primaryPort = new PWM(primaryChannel, _frequency, _primaryDuty, false);
        private static OutputPort _secondaryPort = new OutputPort(Pins.GPIO_PIN_D9, false);

        private static int _secondaryDelayCycles;
        private static int _secondaryOnCycles;
        private static ICycleService cycleService;
        private static IPulseService pulseService;

        public static double Frequency
        {
            get { return _frequency; }
        }

        public static double PrimaryDuty
        {
            get { return _primaryDuty; }
        }

        public static double SecondaryDutyDelay
        {
            get { return _secondaryDelay; }
        }

        public static void Main()
        {
            PopulateTheRegistry();

            cycleService = (ICycleService)Registry.Get(typeof(ICycleService));
            pulseService = (IPulseService)Registry.Get(typeof(IPulseService));

            httpServerThread = new Thread(() =>
            {
                httpServer.Start();
            });
            httpServerThread.Start();

            var calculationThread = new Thread(() =>
            {
                while (true)
                {
                    calculationStartEvent.WaitOne();

                    //StopPWM();
                    CalculateDelays();

                    calculationDone = true;
                }
            });
            calculationThread.Start();

            Thread.Sleep(100);
            cycleService.Calibrate();


            CalculateDelays();

            StartPWM();
        }

        public static void ChangePrimaryPulse(double frequency, double duty)
        {
            _frequency = frequency;
            _primaryDuty = duty;

            calculationStartEvent.Set();

            Thread.Sleep(50);//sleep a little to allow the main thread to restart the pwm correctly
        }

        public static void ChangeSecondaryPulse(double dutyDelay)
        {
            _secondaryDelay = dutyDelay;

            calculationStartEvent.Set();

            Thread.Sleep(50);//sleep a little to allow the main thread to restart the pwm correctly
        }

        private static void PopulateTheRegistry()
        {
            var pulseService = new PulseService();
            Registry.RegisterInstance(typeof(IPulseService), pulseService);
            var cycleService = new CycleService(pulseService);
            Registry.RegisterInstance(typeof(ICycleService), cycleService);
            Registry.RegisterInstance(typeof(IDelayService), new DelayService());
        }

        private static void CalculateDelays()
        {
            double offTime;
            double secondaryDelaySeconds = pulseService.CalculateDurationSeconds(_frequency, _secondaryDelay, out offTime);
            _secondaryDelayCycles = cycleService.ConvertSecondsToForCycles(secondaryDelaySeconds);
            _secondaryOnCycles = cycleService.ConvertSecondsToForCycles(offTime);

            if (_secondaryOnCycles > 1)
            {
                _secondaryOnCycles--;
            }
        }

        private static void StartPWM()
        {
            while (true)
            {
                primaryPort.Frequency = _frequency;
                primaryPort.DutyCycle = _primaryDuty;

                primaryPort.Start();

                while (!calculationDone)
                {
                    for (var i = 0; i < _secondaryDelayCycles; i++)
                    {
                    }
                    _secondaryPort.Write(true);
                    for (var i = 0; i < _secondaryOnCycles; i++)
                    {
                    }
                    _secondaryPort.Write(false);
                }

                calculationDone = false;
                StopPWM();
            }
        }

        private static void StopPWM()
        {
            primaryPort.Stop();
            _secondaryPort.Write(false);
        }

    }
}
