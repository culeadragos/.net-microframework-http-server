﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using NetMFHttpServer;
using NetMFUtils;
using PWMServer.Services;
using PWMServer.Domain;

namespace NetduinoOutputPortPWMServer
{
    public class Program
    {
        private static HttpServer httpServer = new HttpServer(81);
        private static Thread httpServerThread;
        private static AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        private static OutputPort primaryPort = new OutputPort(Pins.GPIO_PIN_D6, false);
        private static double _frequency = 250d;
        private static double _duty = 0.1d;
        private static CyclePulse primaryPortPulse;
        private static int sum;

        public static double Frequency
        {
            get
            {
                return _frequency;
            }
        }

        public static void Main()
        {
            PopulateTheRegistry();

            var cycleService = (CycleService)Registry.Get(typeof(ICycleService));

            httpServerThread = new Thread(() =>
            {
                httpServer.Start();
            });
            httpServerThread.Start();

            var calculationThread = new Thread(() =>
            {
                while (true)
                {
                    autoResetEvent.WaitOne();
                    primaryPortPulse = cycleService.GenerateCyclePulse(_frequency, _duty);
                }
            });
            calculationThread.Start();

            Thread.Sleep(100);
            cycleService.Calibrate();

            primaryPortPulse = cycleService.GenerateCyclePulse(_frequency, _duty);

            while (true)
            {
                primaryPort.Write(true);
                for (var i = 0; i < primaryPortPulse.OnCycles; i++)
                {
                }
                primaryPort.Write(false);
                for (int i = 0; i < primaryPortPulse.OffCycles; ++i)//do the loop here to avoid cycles wasting due to method call
                {
                }
            }
        }

        public static void ChangeFrequencyAndDuty(double frequency, double duty)
        {
            _frequency = frequency;
            _duty = duty;

            autoResetEvent.Set();
        }

        private static void PopulateTheRegistry()
        {
            var pulseService = new PulseService();
            Registry.RegisterInstance(typeof(IPulseService), pulseService);
            var cycleService = new CycleService(pulseService);
            Registry.RegisterInstance(typeof(ICycleService), cycleService);
            Registry.RegisterInstance(typeof(IDelayService), new DelayService());
        }

        private static void CalculateDelays()
        {
            //double offTime;
            //double secondaryDelaySeconds = pulseService.CalculateDurationSeconds(_frequency, _secondaryDelay, out offTime);
            //_secondaryDelayCycles = cycleService.ConvertSecondsToForCycles(secondaryDelaySeconds);
        }
    }
}
