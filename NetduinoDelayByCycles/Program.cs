﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using NetMFHttpServer;
using NetMFUtils;

namespace NetduinoDelayByCycles
{
    public class Program
    {
        private static HttpServer httpServer = new HttpServer(81);
        private static Thread httpServerThread;
        private static AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        private static Cpu.PWMChannel primaryChannel = PWMChannels.PWM_PIN_D6;
        private static Cpu.PWMChannel secondaryChannel = PWMChannels.PWM_PIN_D9;
        private static double _frequency = 250d;
        private static double _primaryDuty = 0.1d;
        private static double _secondaryDuty = 0.15;
        private static int _secondaryDelayCycles = 10;

        private static Microsoft.SPOT.Hardware.PWM primaryPort = new PWM(primaryChannel, _frequency, _primaryDuty, false);
        private static Microsoft.SPOT.Hardware.PWM secondaryPort = new PWM(secondaryChannel, _frequency, _secondaryDuty, false);

        public static double Frequency
        {
            get { return _frequency; }
        }

        public static double PrimaryDuty
        {
            get { return _primaryDuty; }
        }

        public static int SecondaryDelayCycles
        {
            get { return _secondaryDelayCycles; }
        }

        public static double SecondaryDuty
        {
            get { return _secondaryDuty; }
        }

        public static void Main()
        {
            //PopulateTheRegistry();

            //cycleService = (ICycleService)Registry.Get(typeof(ICycleService));
            //pulseService = (IPulseService)Registry.Get(typeof(IPulseService));

            httpServerThread = new Thread(() =>
            {
                httpServer.Start();
            });
            httpServerThread.Start();

            var resetThread = new Thread(() =>
            {
                while (true)
                {
                    autoResetEvent.WaitOne();

                    StopPWM();
                    StartPWM();
                }
            });
            resetThread.Start();

            StartPWM();

            Thread.Sleep(Timeout.Infinite);
        }

        public static void ChangePrimaryPulse(double frequency, double duty)
        {
            _frequency = frequency;
            _primaryDuty = duty;

            autoResetEvent.Set();

            //Thread.Sleep(10);//sleep a little to allow the main thread to restart the pwm correctly
        }

        public static void ChangeSecondaryPulse(int delayCycles, double duty)
        {
            _secondaryDuty = duty;
            _secondaryDelayCycles = delayCycles;

            autoResetEvent.Set();

            //Thread.Sleep(10);//sleep a little to allow the main thread to restart the pwm correctly
        }

        //private static void PopulateTheRegistry()
        //{
        //    var pulseService = new PulseService();
        //    Registry.RegisterInstance(typeof(IPulseService), pulseService);
        //    var cycleService = new CycleService(pulseService);
        //    Registry.RegisterInstance(typeof(ICycleService), cycleService);
        //    Registry.RegisterInstance(typeof(IDelayService), new DelayService());
        //}

        private static void StartPWM()
        {
            primaryPort.Frequency = _frequency;
            primaryPort.DutyCycle = _primaryDuty;
            secondaryPort.Frequency = _frequency;
            secondaryPort.DutyCycle = _secondaryDuty;

            primaryPort.Start();
            for (var i = 0; i < _secondaryDelayCycles; i++)
            {
            }
            secondaryPort.Start();
        }

        private static void StopPWM()
        {
            primaryPort.Stop();
            secondaryPort.Stop();
        }

    }
}
