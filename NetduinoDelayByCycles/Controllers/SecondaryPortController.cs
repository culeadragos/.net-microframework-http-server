using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using PWMServer.Domain;

namespace NetduinoDelayByCycles.Controllers
{
    public class SecondaryPortController : BaseController
    {
        public void ChangePulse(string dutyDelay, string duty)
        {
            int delayCyclesAsInt;

            try
            {
                delayCyclesAsInt = int.Parse(dutyDelay);
            }
            catch(Exception)
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangeSecondaryPulse(delayCyclesAsInt, dutyAsDouble);
        }

        public DelayStatus IncreaseDelayBy(string value)
        {
            var delayCycles = int.Parse(value);

            var newDelay = Program.SecondaryDelayCycles + delayCycles;

            var result = new DelayStatus(Program.Frequency, Program.PrimaryDuty, newDelay, Program.SecondaryDuty);

            Program.ChangeSecondaryPulse(newDelay, Program.SecondaryDuty);

            return result;
        }

        public DelayStatus IncreaseDutyBy(string value)
        {
            var dutyAsDouble = double.Parse(value);

            var newDuty = Program.SecondaryDuty + dutyAsDouble;
            if (newDuty >= 0.999 || newDuty < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new DelayStatus(Program.Frequency, Program.PrimaryDuty, Program.SecondaryDelayCycles, newDuty);

            Program.ChangeSecondaryPulse(Program.SecondaryDelayCycles, newDuty);

            return result;
        }
    }
}
