using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using PWMServer.Domain;

namespace NetduinoDelayByCycles.Controllers
{
    public class PwmController : BaseController
    {
        public DelayStatus GetStatus()
        {
            return new DelayStatus(Program.Frequency, Program.PrimaryDuty, Program.SecondaryDelayCycles, Program.SecondaryDuty);
        }
    }
}
