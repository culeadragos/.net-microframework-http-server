using System;
using Microsoft.SPOT;
using NetMFUtils;
using NetMFHttpServer.Exceptions;
using PWMServer.Domain;
using NetMFHttpServer;

namespace NetduinoDelayByCycles.Controllers
{
    public class PrimaryPortController : BaseController
    {
        public void ChangePulse(string frequency, string duty)
        {
            double frequencyAsDouble;
            if (!PwmParsing.TryParseFrequency(frequency, out frequencyAsDouble))
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangePrimaryPulse(frequencyAsDouble, dutyAsDouble);
        }

        public DelayStatus IncreaseFrequencyBy(string value)
        {
            var frequencyAsDouble = double.Parse(value);

            var newFrequency = Program.Frequency + frequencyAsDouble;
            if (newFrequency >= 24000000 || newFrequency < 0.1)
            {
                throw new BadRequestException();
            }

            var result = new DelayStatus(newFrequency, Program.PrimaryDuty, Program.SecondaryDelayCycles, Program.SecondaryDuty);

            Program.ChangePrimaryPulse(newFrequency, Program.PrimaryDuty);

            return result;
        }

        public DelayStatus IncreaseDutyBy(string value)
        {
            var dutyyAsDouble = double.Parse(value);

            var newDuty = Program.PrimaryDuty + dutyyAsDouble;
            if (newDuty >= 0.999 || newDuty < 0.0005d)
            {
                throw new BadRequestException();
            }

            var result = new DelayStatus(Program.Frequency, newDuty, Program.SecondaryDelayCycles, Program.SecondaryDuty);

            Program.ChangePrimaryPulse(Program.Frequency, newDuty);

            return result;
        }
    }
}
