using System;
using System.Collections;

namespace NetMFUtils
{
    public static class Registry
    {
        public static Hashtable store = new Hashtable();

        public static void RegisterInterface(Type type, Object objectOfType)
        {
            var interfaces = objectOfType.GetType().GetInterfaces();
            var found = false;

            foreach (var interfaceType in  interfaces)
            {
                if (interfaceType.Equals(type))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                throw new ArgumentException("The type of the object does not implement type");
            }

            store.Add(type, objectOfType);
        }

        public static void RegisterInstance(Type type, Object objectOfType)
        {
            store.Add(type, objectOfType);
        }

        public static Object Get(Type type)
        {
            return store[type];
        }
    }
}
