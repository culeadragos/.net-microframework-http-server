using System;
using Microsoft.SPOT;
using System.Diagnostics;
using Microsoft.SPOT.Hardware;
using static Microsoft.SPOT.Hardware.Cpu;

namespace NetMFUtils.CycleTimers
{
    public class WriteCycleTimer:CycleTimer
    {
        bool state = false;
        private OutputPort port = new OutputPort(Pin.GPIO_Pin0, false);

        private double secondsPerCycle;

        public double SecondsPerCycle
        {
            get
            {
                return secondsPerCycle;
            }
        }

        public WriteCycleTimer()
        {

        }

        public override void Calibrate()
        {
            //DateTime startTime = DateTime.Now;
            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < CYCLE_COUNT; ++i)
            {
                port.Write(!state);
            }
            //DateTime endTime = DateTime.Now;
            //TimeSpan timeDifference = endTime.Subtract(startTime);
            //_cyclesPerSecond = ((double)CYCLE_COUNT / (double)timeDifference.Ticks) * 10000000d;

            stopWatch.Stop();
            cyclesPerTick = (double)CYCLE_COUNT / (double)stopWatch.Ticks;
            var ticksPerCycle = (double)stopWatch.Ticks / (double)CYCLE_COUNT;

            secondsPerCycle = ticksPerCycle / (ticksPerMillisecond * 1000d);
        }
    }
}
