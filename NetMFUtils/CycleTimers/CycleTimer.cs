using System;
using Microsoft.SPOT;

namespace NetMFUtils.CycleTimers
{
    public abstract class CycleTimer
    {
        protected const int CYCLE_COUNT = 100000;//1048576;
        protected double cyclesPerTick = 112262.2255516001d;
        protected StopWatch stopWatch = new StopWatch();

        protected double ticksPerMillisecond = (double)TimeSpan.TicksPerMillisecond;

        public double CyclesPerTick
        {
            get { return cyclesPerTick; }
        }

        public double CyclesPerMicrosecond
        {
            get
            {
                return cyclesPerTick * ticksPerMillisecond / 1000d;
            }
        }

        public double CyclesPerNanosecond
        {
            get
            {
                return cyclesPerTick * ticksPerMillisecond / 1000000d;
            }
        }

        public double CyclesPerMillisecond
        {
            get
            {
                return cyclesPerTick * ticksPerMillisecond;
            }
        }

        public double CyclesPerSecond
        {
            get
            {
                return cyclesPerTick * ticksPerMillisecond * 1000;
            }
        }

        public abstract void Calibrate();
    }
}
