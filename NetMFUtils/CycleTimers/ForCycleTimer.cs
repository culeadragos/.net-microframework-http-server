using System;
using Microsoft.SPOT;

namespace NetMFUtils.CycleTimers
{
    public class ForCycleTimer : CycleTimer
    {
        private double secondsPerCycle;

        public double SecondsPerCycle
        {
            get
            {
                return secondsPerCycle;
            }
        }

        public override void Calibrate()
        {
            //DateTime startTime = DateTime.Now;
            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < CYCLE_COUNT; ++i)
            {
            }
            //assign = 1;
            //DateTime endTime = DateTime.Now;
            //TimeSpan timeDifference = endTime.Subtract(startTime);
            //_cyclesPerSecond = ((double)CYCLE_COUNT / (double)timeDifference.Ticks) * 10000000d;

            stopWatch.Stop();
            cyclesPerTick = (double)CYCLE_COUNT / (double)stopWatch.Ticks;
            //cyclesPerTick = 1d / (double)stopWatch.Ticks;
            var ticksPerCycle = (double)stopWatch.Ticks / (double)CYCLE_COUNT;

            secondsPerCycle = ticksPerCycle / (ticksPerMillisecond * 1000d);
        }
    }
}
