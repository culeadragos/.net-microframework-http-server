using System;
using Microsoft.SPOT;
using System.Diagnostics;

namespace NetMFUtils.CycleTimers
{
    public class SumCycleTimer:CycleTimer
    {
        private int sum;

        public override void Calibrate()
        {
            sum = 0;
            //DateTime startTime = DateTime.Now;
            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < CYCLE_COUNT; ++i)
            {
                ++sum;
            }
            //DateTime endTime = DateTime.Now;
            //TimeSpan timeDifference = endTime.Subtract(startTime);
            //_cyclesPerSecond = ((double)CYCLE_COUNT / (double)timeDifference.Ticks) * 10000000d;

            stopWatch.Stop();
            cyclesPerTick = (double)CYCLE_COUNT / (double)stopWatch.Ticks;
            //cyclesPerTick = 1d / (double)stopWatch.Ticks;
        }

        //public void WaitSeconds(double seconds)
        //{
        //    int cycleCount = (int)((seconds * CyclesPerSecond));
        //    int dummyValue = 0;
        //    for (int i = 0; i < cycleCount; ++i)
        //    {
        //        ++dummyValue;
        //    }
        //}

        //public void WaitMilliseconds(double milliseconds)
        //{
        //    int cycleCount = (int)(milliseconds * CyclesPerTick / 1000d);
        //    int dummyValue = 0;
        //    for (int i = 0; i < cycleCount; ++i)
        //    {
        //        ++dummyValue;
        //    }
        //}

        //public void WaitMicroseconds(double microseconds)
        //{
        //    int cycleCount = (int)(microseconds * CyclesPerTick / 1000000d);
        //    int dummyValue = 0;
        //    for (int i = 0; i < cycleCount; ++i)
        //    {
        //        ++dummyValue;
        //    }
        //}


    }
}
