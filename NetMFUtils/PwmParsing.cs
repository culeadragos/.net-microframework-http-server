using System;
using Microsoft.SPOT;

namespace NetMFUtils
{
    public static class PwmParsing
    {
        public static bool TryParseFrequency(string frequency, out double frequencyAsDouble)
        {
            if (! double.TryParse(frequency, out frequencyAsDouble))
            {
                return false;
            }

            if (frequencyAsDouble < 1)
            {
                return false;
            }

            return true;
        }

        public static bool TryParseDuty(string duty, out double dutyAsDouble)
        {
            if (!double.TryParse(duty, out dutyAsDouble))
            {
                return false;
            }

            if (!IsDutyValid(dutyAsDouble))
            {
                return false;
            }

            return true;
        }

        public static bool IsDutyValid(double duty)
        {
            return (duty > 0.00025 && duty < 0.999);
        }

        public static bool TryParseTime(string time, out double timeAsDouble)
        {
            if (!double.TryParse(time, out timeAsDouble))
            {
                return false;
            }

            if (timeAsDouble < 0.000001)
            {
                return false;
            }

            return true;
        }
    }
}
