using System;
using Microsoft.SPOT;

namespace NetMFUtils
{
    public class StopWatch
    {
        private long startTicks = 0;
        private long stopTicks = 0;
        private bool isRunning = false;

        private const long ticksPerMillisecond = System.TimeSpan.TicksPerMillisecond;

        public StopWatch()
        {
        }

        public void Reset()
        {
            startTicks = 0;
            stopTicks = 0;
            isRunning = false;
        }

        public void Start()
        {
            if (isRunning)
            {
                throw new InvalidOperationException("Already running");
            }

            isRunning = true;
            startTicks = Microsoft.SPOT.Hardware.Utility.GetMachineTime().Ticks; // start new timer
        }

        public void Stop()
        {
            stopTicks = Microsoft.SPOT.Hardware.Utility.GetMachineTime().Ticks;
            isRunning = false;
        }

        public long Ticks
        {
            get
            {
                return stopTicks - startTicks;
            }
        }

        public long ElapsedMilliseconds
        {
            get
            {
                if (isRunning)
                {
                    throw new InvalidOperationException("Still running");
                }

                return (stopTicks - startTicks) / ticksPerMillisecond;
            }
        }

        public double ElapsedSeconds
        {
            get
            {
                if (isRunning)
                {
                    throw new InvalidOperationException("Still running");
                }

                TimeSpan duration = new TimeSpan((stopTicks - startTicks));
                return duration.Seconds;
            }
        }

        public double ElapsedMinutes
        {
            get
            {
                if (isRunning)
                {
                    throw new InvalidOperationException("Still running");
                }

                TimeSpan duration = new TimeSpan((stopTicks - startTicks));
                return duration.Minutes;
            }
        }
    }
}
