using System;
using Microsoft.SPOT;
using NetMFHttpServer.UnitTests.Domain;

namespace NetMFHttpServer.UnitTests.Controllers
{
    public class TestController:BaseController
    {
        public void DoSomething()
        {
            
        }

        public int GetAnInt()
        {
            return 3;
        }

        public TestClass GetTestObject()
        {
            return new TestClass {Abc = 4, Def = "xyz", OtherTest = new OtherTestClass {DateValue = DateTime.Now}};
        }

        /// <summary>
        /// All the parameters must be strings because .NETMF Reflection does not know how to read the parameters of a method
        /// </summary>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <returns></returns>
        public DateTime WrongGetDate(string param1, int param2)
        {
            return DateTime.Now;
        }

        /// <summary>
        /// All the parameters must be strings because .NETMF Reflection does not know how to read the parameters of a method
        /// </summary>
        /// <param name="testObject"></param>
        /// <returns></returns>
        public TestClass[] WrongGetArray(TestClass testObject)
        {
            return new TestClass[]
            {
                testObject,
                new TestClass {Abc = 4, Def = "xyz", OtherTest = new OtherTestClass {DateValue = DateTime.Now}}
            };
        }

        public int[] WrongGetInts(int a, int b, int c)
        {
            return new[] {a, b, c};
        }

        /// <summary>
        /// Proper way to define an action with parameters
        /// </summary>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <returns></returns>
        public DateTime GetDate(string param1, string param2)
        {
            return DateTime.Now;
        }

        
        public TestClass[] GetArray(string testObjectAsJson)
        {
            var testObject = DeserializeJson(testObjectAsJson);//this returns a hashtable, it's impossible to get anything strongly typed
            //cannot do a cast, the entire converting must be done manually

            return new TestClass[]
            {
                new TestClass {Abc = 4, Def = "xyz", OtherTest = new OtherTestClass {DateValue = DateTime.Now}}
            };
        }

        public int[] GetInts(string a, string b, string c)
        {
            return new[] { int.Parse(a), int.Parse(b), int.Parse(c) };
        }
    }
}
