﻿using System;
using System.Net;
using Microsoft.SPOT;
using Json.NETMF;
using NetMFHttpServer.Model;
using NetMFHttpServer.UnitTests.Controllers;
using NetMFHttpServer.UnitTests.Domain;

namespace NetMFHttpServer.UnitTests
{
    /// <summary>
    /// Debug this program to run the tests. Unfortunately .Net Microframework does not have support for unit testing, so this is the first thing I could think of
    /// </summary>
    public class Program
    {
        private static readonly ControllerTypeParser _controllerTypeParser = new ControllerTypeParser();
        private static  ControllerAssemblyParser _controllerAssemblyParser;
        private static RouteTable _routeTable;
        private static HttpServer _httpServer = new HttpServer(81);
        private static JsonSerializer _jsonSerializer = new JsonSerializer();

        public static void Main()
        {
            TestSetup();

            DoTests();

            TestTearDown();
        }

        private static void TestSetup()
        {
            _controllerAssemblyParser = new ControllerAssemblyParser(_controllerTypeParser);

            _routeTable = _controllerAssemblyParser.PopulateRouteTable();
            Debug.Print("Start executing tests..");
        }

        /// <summary>
        /// If there is any exception thrown in here then the tests are failing
        /// </summary>
        private static void DoTests()
        {
            HttpStatusCode statusCode;
            var result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.DoSomething), Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.DoSomething) + " failed", result==null && statusCode == HttpStatusCode.OK);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.GetAnInt), Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.GetAnInt) + " failed", (int)result == 3 && statusCode == HttpStatusCode.OK);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.GetTestObject), Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.GetTestObject) + " failed", result != null && statusCode == HttpStatusCode.OK);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.WrongGetDate) + "?param1=1&param2=2", Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.WrongGetDate) + " failed", statusCode == HttpStatusCode.BadRequest);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.WrongGetArray), Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.WrongGetArray) + " failed" , statusCode == HttpStatusCode.BadRequest);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.WrongGetInts) + "?a=1&b=2&c=3", Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.WrongGetInts) + " failed", statusCode == HttpStatusCode.BadRequest);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.WrongGetInts) + "?a=1&b=2&c=3", Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.WrongGetInts) + " failed", statusCode == HttpStatusCode.BadRequest);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.GetInts) + "?a=1&b=2&c=3", Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.GetInts) + " failed", statusCode == HttpStatusCode.OK);

            result = _httpServer.HandleRoute(_routeTable, "/test/" + nameof(TestController.GetDate) + "?a=1&b=2", Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.GetDate) + " failed", statusCode == HttpStatusCode.OK);

            var testObject = new TestClass {Abc = 77, Def = "uio", OtherTest = new OtherTestClass {DateValue = DateTime.Now}};
            var testObjectAsJson = _jsonSerializer.Serialize(testObject);
            result = _httpServer.HandleRoute(_routeTable, "/test/"+ nameof(TestController.GetArray)+"?param=" + testObjectAsJson, Verb.GET, string.Empty, out statusCode);
            ThrowIfNot(nameof(TestController) + "." + nameof(TestController.GetArray) + " failed", statusCode == HttpStatusCode.OK);
        }

        private static void ThrowIfNot(string message, bool condition)
        {
            if (!condition)
            {
                throw new Exception(message);
            }
        }

        private static void TestTearDown()
        {
            Debug.Print("Finished executing tests");
        }
    }
}
