namespace NetMFHttpServer.UnitTests.Domain
{
    public class TestClass
    {
        public int Abc { get; set; }

        public string Def { get; set; }

        public OtherTestClass OtherTest { get; set; }
    }
}
