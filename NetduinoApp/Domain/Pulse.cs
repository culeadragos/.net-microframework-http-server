using System;
using Microsoft.SPOT;

namespace NetduinoApp.Domain
{
    public class Pulse
    {
        public double Frequency { get; private set; }

        public double Duty { get; private set; }

        public Pulse(double frequency, double duty)
        {
            this.Frequency = frequency;
            this.Duty = duty;
        }
    }
}
