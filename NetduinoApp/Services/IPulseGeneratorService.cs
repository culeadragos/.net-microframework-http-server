﻿using NetduinoApp.Domain;

namespace NetduinoApp.Services
{
    public interface IPulseGeneratorService
    {
        Pulse GeneratePulseByDuty(double timeOn, double duty);
        Pulse GeneratePulseByFrequency(double timeOn, double frequency);
    }
}