using System;
using Microsoft.SPOT;
using NetduinoApp.Domain;

namespace NetduinoApp.Services
{
    public class PulseGeneratorService : IPulseGeneratorService
    {
        public Pulse GeneratePulseByFrequency(double timeOn, double frequency)
        {
            var period = 1d / frequency;

            var duty = timeOn / period;

            return new Pulse(frequency, duty);
        }

        public Pulse GeneratePulseByDuty(double timeOn, double duty)
        {
            var period = timeOn / duty;

            var frequency = 1d / period;

            return new Pulse(frequency, duty);
        }
    }
}
