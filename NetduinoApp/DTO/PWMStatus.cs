using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace NetduinoApp.DTO
{
    public class PWMStatus
    {
        public double Frequency { get; set; }

        public double Duty { get; set; }

        public Cpu.PWMChannel PIN { get; set; }

        public Cpu.PWMChannel SecondaryPIN { get; set; }

        public double SecondaryDuty { get; set; }
    }
}
