using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using NetMFHttpServer;
using NetMFHttpServer.Exceptions;
using NetduinoApp.DTO;
using NetduinoApp.Services;
using NetMFUtils;

namespace NetduinoApp.Controllers
{
    public class PwmController:BaseController
    {
        

        public PwmController()
        {

        }

        /// <summary>
        /// Changes the frequency, duty of the PWM thread
        /// </summary>
        /// <param name="frequency">In Hz</param>
        /// <param name="duty">A value between 0.00025 and 0.999</param>
        public void SetFrequencyAndDuty(string frequency, string duty)
        {
            double frequencyAsDouble;
            if (!PwmParsing.TryParseFrequency(frequency, out frequencyAsDouble))
            {
                throw new BadRequestException();
            }

            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangePwm(frequencyAsDouble, dutyAsDouble);
        }

        public void Stop()
        {
            Program.StopPwm();
        }

        public void Start()
        {
            Program.StartPwm();
        }

        public void IncreaseFrequencyBy(string value)
        {
            var frequencyAsDouble = double.Parse(value);

            var newFrequency = Program.Frequency + frequencyAsDouble;
            if (newFrequency >= 24000000)
            {
                throw new BadRequestException();
            }

            Program.ChangePwm(newFrequency, Program.Duty);
        }

        public void DecreaseFrequencyBy(string value)
        {
            var frequencyAsDouble = double.Parse(value);

            var newFrequency = Program.Frequency - frequencyAsDouble;
            if (newFrequency < 0.1)
            {
                throw new BadRequestException();
            }

            Program.ChangePwm(newFrequency, Program.Duty);
        }

        public void IncreaseDutyBy(string value)
        {
            var dutyyAsDouble = double.Parse(value);

            var newDuty = Program.Duty + dutyyAsDouble;
            if (newDuty >= 0.999)
            {
                throw new BadRequestException();
            }

            Program.ChangePwm(Program.Frequency, newDuty);
        }

        public void DecreaseDutyBy(string value)
        {
            var dutyAsDouble = double.Parse(value);

            var newDuty = Program.Duty - dutyAsDouble;
            if (newDuty < 0.0005d)
            {
                throw new BadRequestException();
            }

            Program.ChangePwm(Program.Frequency, newDuty);
        }

        public void SetPIN(string pin)
        {
            var pinAsInt = int.Parse(pin);
            Cpu.PWMChannel newPIN;

            switch (pinAsInt)
            {
                case 3:
                    newPIN = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D3;
                    break;

                case 5:
                    newPIN = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D5;
                    break;

                case 6:
                    newPIN = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D6;
                    break;

                case 9:
                    newPIN = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D9;
                    break;

                case 10:
                    newPIN = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D10;
                    break;

                case 11:
                    newPIN = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D11;
                    break;

                default:
                    throw new BadRequestException();
            }
            Program.PIN = newPIN;
        }

        public PWMStatus Status()
        {
            return new PWMStatus { Frequency = Program.Frequency, Duty = Program.Duty, PIN = Program.PIN,
                //SecondaryPIN = Program.SecondaryPIN, SecondaryDuty = Program.SecondaryDuty

            };
        }
    }
}
