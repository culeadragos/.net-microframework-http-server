using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using PWMServer.Service;
using NetMFUtils;

namespace NetduinoApp.Controllers
{
    public class FiddleController:BaseController
    {
        private readonly IFiddleService fiddleService = (IFiddleService)Registry.Get(typeof(IFiddleService));

        public void Start()
        {
            fiddleService.NewValue += (sender, args) =>
            {
                Program.ChangePwm(args.Frequency, args.Duty);
            };
            fiddleService.Start();
        }

        public void Pause()
        {
            fiddleService.Pause();
        }

        public void Resume()
        {
            fiddleService.Resume();
        }

        public void Stop()
        {
            fiddleService.Stop();
        }

        public void IncreaseDuty()
        {
            fiddleService.IncreaseDuty();
        }
    }
}
