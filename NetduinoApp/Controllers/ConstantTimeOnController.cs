using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using NetduinoApp.Services;
using NetMFHttpServer.Exceptions;
using NetduinoApp.DTO;
using NetMFUtils;

namespace NetduinoApp.Controllers
{
    public class ConstantTimeOnController : BaseController
    {
        private readonly IPulseGeneratorService pulseGeneratorService = (IPulseGeneratorService)Registry.Get(typeof(IPulseGeneratorService));
        private static double timeOnPeriod;

        public ConstantTimeOnController()
        {

        }

        public PWMStatus Set(string timeOn, string frequency)
        {
            double frequencyAsDouble;
            if (!PwmParsing.TryParseFrequency(frequency, out frequencyAsDouble))
            {
                throw new BadRequestException();
            }

            double timeOnAsDouble;
            if (!PwmParsing.TryParseTime(timeOn, out timeOnAsDouble))
            {
                throw new BadRequestException();
            }

            var pulse = pulseGeneratorService.GeneratePulseByFrequency(timeOnAsDouble, frequencyAsDouble);

            timeOnPeriod = timeOnAsDouble;

            Program.ChangePwm(pulse.Frequency, pulse.Duty);

            return new PWMStatus { Frequency = pulse.Frequency, Duty = pulse.Duty, PIN = Program.PIN };
        }

        public PWMStatus SetWithDuty(string timeOn, string duty)
        {
            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            double timeOnAsDouble;
            if (!PwmParsing.TryParseTime(timeOn, out timeOnAsDouble))
            {
                throw new BadRequestException();
            }

            var pulse = pulseGeneratorService.GeneratePulseByDuty(timeOnAsDouble, dutyAsDouble);

            timeOnPeriod = timeOnAsDouble;

            Program.ChangePwm(pulse.Frequency, pulse.Duty);

            return new PWMStatus { Frequency = pulse.Frequency, Duty = pulse.Duty, PIN = Program.PIN };
        }

        public void IncreaseFrequency(string byHz)
        {
            double byHzAsDouble;
            if (!double.TryParse(byHz, out byHzAsDouble) || Program.Frequency + byHzAsDouble < 1)
            {
                throw new BadRequestException();
            }

            var newFrequency = Program.Frequency + byHzAsDouble;

            var pulse = pulseGeneratorService.GeneratePulseByFrequency(timeOnPeriod, newFrequency);

            Program.ChangePwm(pulse.Frequency, pulse.Duty);
        }

        
        public void IncreaseDuty(string by)
        {
            double byAsDouble;
            if (!double.TryParse(by, out byAsDouble))
            {
                throw new BadRequestException();
            }

            var newDuty = Program.Duty + byAsDouble;

            if (!PwmParsing.IsDutyValid(newDuty))
            {
                throw new BadRequestException();
            }

            var pulse = pulseGeneratorService.GeneratePulseByDuty(timeOnPeriod, newDuty);

            Program.ChangePwm(pulse.Frequency, pulse.Duty);
        }
    }
}
