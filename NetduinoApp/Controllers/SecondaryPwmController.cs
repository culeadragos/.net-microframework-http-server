using System;
using Microsoft.SPOT;
using NetMFHttpServer;
using NetMFHttpServer.Exceptions;
using NetduinoApp.Services;
using NetMFUtils;

namespace NetduinoApp.Controllers
{
    public class SecondaryPwmController:BaseController
    {
        
        


        public void SetDuty(string duty)
        {
            double dutyAsDouble;
            if (!PwmParsing.TryParseDuty(duty, out dutyAsDouble))
            {
                throw new BadRequestException();
            }

            Program.ChangeSecondaryPwm(Program.Frequency, dutyAsDouble);
        }

        //public void IncreaseDutyBy(string by)
        //{
        //    var dutyyAsDouble = double.Parse(by);

        //    var newDuty = Program.SecondaryDuty + dutyyAsDouble;
        //    if (newDuty >= 0.999)
        //    {
        //        throw new BadRequestException();
        //    }

        //    Program.ChangeSecondaryPwm(Program.Frequency, newDuty);
        //}

        //public void DecreaseDutyBy(string value)
        //{
        //    var dutyAsDouble = double.Parse(value);

        //    var newDuty = Program.SecondaryDuty - dutyAsDouble;
        //    if (newDuty < 0.0005d)
        //    {
        //        throw new BadRequestException();
        //    }

        //    Program.ChangeSecondaryPwm(Program.Frequency, newDuty);
        //}
    }
}
