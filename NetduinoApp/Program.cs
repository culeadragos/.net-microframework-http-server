﻿using System;
using System.Threading;
using Microsoft.SPOT.Hardware;
using NetMFHttpServer;
using Microsoft.SPOT;
using NetduinoApp.Services;
using NetMFUtils;
using PWMServer.Service;

namespace NetduinoApp
{
    public class Program:IDisposable
    {
        private static HttpServer _httpServer = new HttpServer(81);
        private static Thread _httpServerThread;
        private static Microsoft.SPOT.Hardware.PWM _pwmPort;
        //private static Microsoft.SPOT.Hardware.PWM _secondaryPwmPort;
        private static AutoResetEvent _autoResetEvent = new AutoResetEvent(false);
        private static Cpu.PWMChannel _pwmPin = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D6;
        //private static Cpu.PWMChannel _pwmSecondaryPin = SecretLabs.NETMF.Hardware.Netduino.PWMChannels.PWM_PIN_D9;
        private static double _frequency = 250d;
        private static double _duty = 0.0005d;
        private static double _secondaryDuty = 0.99d;

        public static double Frequency
        {
            get
            {
                return _frequency;
            }
        }

        public static double Duty
        {
            get
            {
                return _duty;
            }
        }

        //public static double SecondaryDuty
        //{
        //    get
        //    {
        //        return _secondaryDuty;
        //    }
        //}

        public static Cpu.PWMChannel PIN
        {
            get
            {
                return _pwmPin;
            }
            set
            {
                _pwmPin = value;
            }
        }

        //public static Cpu.PWMChannel SecondaryPIN
        //{
        //    get
        //    {
        //        return _pwmSecondaryPin;
        //    }
        //    set
        //    {
        //        _pwmSecondaryPin = value;
        //    }
        //}

        public static void Main()
        {
            
            Registry.RegisterInterface(typeof(IPulseGeneratorService), new PulseGeneratorService());
            Registry.RegisterInterface(typeof(IFiddleService), new FiddleService());

            _httpServerThread = new Thread(() =>
            {
                _httpServer.Start();
            });
            _httpServerThread.Start();

            _pwmPort = new Microsoft.SPOT.Hardware.PWM(_pwmPin, _frequency, _duty, false);//1 = 100%,  - minimum
            //_secondaryPwmPort = new PWM(_pwmSecondaryPin, _frequency, _secondaryDuty, false);
            _pwmPort.Start();
            //_secondaryPwmPort.Start();

            while (true)
            {
                _autoResetEvent.WaitOne();

                try
                {
                    _pwmPort.Frequency =
                        //_secondaryPwmPort.Frequency = 
                        _frequency;
                    _pwmPort.DutyCycle = _duty;
                    //_secondaryPwmPort.DutyCycle = _secondaryDuty;
                }
                catch(Exception ex)
                {
                    Debug.Print(ex.Message + " | " + ex.StackTrace);
                }
            }
        }

        public static void ChangePwm(double frequency, double duty)
        {
            _frequency = frequency;
            _duty = duty;
            _autoResetEvent.Set();
        }

        public static void ChangeSecondaryPwm(double frequency, double duty)
        {
            _frequency = frequency;
            _secondaryDuty = duty;
            _autoResetEvent.Set();
        }

        public static void StopPwm()
        {
            _pwmPort.Stop();
            //_secondaryPwmPort.Stop();
        }

        public static void StartPwm()
        {
            _pwmPort.Start();
            //_secondaryPwmPort.Start();
        }

        public void Dispose()
        {
            if (_pwmPort != null)
            {
                _pwmPort.Dispose();
            }

            //if (_secondaryPwmPort != null)
            //{
            //    _secondaryPwmPort.Dispose();
            //}
        }
    }
}
